import popuphotkey from './popuphotkey'

const Hotkey = {}
let tmpHotkey = null
let globalStore = null

const hotkeyFocus = () => {
	tmpHotkey = Object.assign({}, globalStore.state.hotkey.shortkey)
	let tmp = Object.assign({}, globalStore.state.hotkey.shortkey)
	for (let key in globalStore.state.hotkey.shortkey) {
		tmp[key] = null
	}
	globalStore.commit('hotkey', tmp)
}

const hotkeyBlur = () => {
	globalStore.commit('hotkey', tmpHotkey)
}

const initial = (el, value) => {
	if (value) {
		createEvent(el)
	} else {
		removeEvent(el)
	}
}

const createEvent = (el) => {
	removeEvent(el)
	el.addEventListener('focus', hotkeyFocus)
	el.addEventListener('blur', hotkeyBlur)
}

const removeEvent = (el) => {
	el.addEventListener('focus', hotkeyFocus)
	el.addEventListener('blur', hotkeyBlur)
}

const defaultStore = {
	state: {
	},
	mutations: {
    hotkey (state, hotkey) {
      state.shortkey = hotkey
    }
	},
	actions: {
		hotkey(context, obj) {
			let replace = {}
			replace[obj.key] = obj.value
			context.commit('hotkey', Object.assign(context.state.shortkey, replace))
		},
		replacehotkey(context, obj) {
			context.commit('hotkey', obj)
		}
	}
}

Hotkey.install = (Vue, store, defaultHotkey, options) => {
	if (!store) {
		throw new Error("Please provide vuex store.");
	}
	defaultStore['state']['shortkey'] = defaultHotkey
	store.registerModule('hotkey', defaultStore)
	globalStore = store
	Vue.directive('hotkey', {
		bind: (el, binding, vnode) => {
			if (!binding.value) {
				return
			}
			if (vnode.tag === 'input') {
				initial(el, true)
			} else {
				hotkeyFocus()
			}
		},
		update: (el, binding, vnode) => {
			if (binding.oldValue === binding.value) {
				return
			}
			if (vnode.tag === 'input') {
				initial(el, binding.value)
			} else {
				if (binding.value) {
					hotkeyFocus()
				} else {
					hotkeyBlur()
				}
			}
		},
		unbind: (el, binding) => {
			if (el.tagName === 'INPUT') {
				removeEvent(el)
			} else {
				if (binding.value !== 'create') {
					hotkeyBlur()
				}
			}
		}
	})
}

export {
	popuphotkey
}
export default Hotkey
