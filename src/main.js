// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Hotkey, { popuphotkey } from './hotkey.js'

Vue.config.productionTip = false

Vue.component("popup-hotkey", popuphotkey)
Vue.use(require('vue-shortkey'))

const hotkey = {
  test1: ['ctrl', 's'],
  test2: null,
  test3: null,
  test4: null,
  test5: null
}
Vue.use(Hotkey, store, hotkey)

/* eslint-disable no-new */
new Vue({
  el: '#app',
	router,
	store,
  components: { App },
  template: '<App/>'
})
